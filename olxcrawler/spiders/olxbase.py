import re, json

from urlparse import urljoin
from math import ceil

from scrapy import log
from scrapy.spider import Spider
from scrapy.http import Request, FormRequest
from scrapy.selector import Selector
from scrapy.exceptions import CloseSpider


REMOVE_NONALPHA = re.compile(r"[^A-Za-z0-9]")


class OlxBase(Spider):
    """ OLX.ph base crawler class for listings
    """

    name = 'olxbase'
    allowed_domains = ['olx.ph']
    search_params = {}
    numresults = 0
    location = ''

    start_urls = []
    base_url = ""


    def __init__(self, *args, **kwargs):
        super(OlxBase, self).__init__(*args, **kwargs)

        self.exclude_words = []

        if not self.base_url:
            raise CloseSpider("Base URL for category not set")

        if 'keyword'in kwargs:
            self.search_params['keywords'] = kwargs['keyword']

        if 'location'in kwargs:
            self.location = kwargs['location']

        if 'exclude'in kwargs:
            exclude = [key.strip() for key in kwargs['exclude'].split(',')]
            self.exclude_words += exclude

        if 'price' in kwargs:
            price = (kwargs['price'] + '-').split('-')[0:2]
            self.search_params['priceFrom'] = price[0]
            self.search_params['priceTo'] = price[1]


    def parse(self, response):
        sel = Selector(response)
        regions = sel.xpath("//script[contains(text(),'_hs_options')]/text()")
        form_token = sel.xpath("//script[contains(text(),'FormToken')]/text()")
        search = re.search(r"FormToken.*=\"(.*)\";", form_token.extract()[0])
        search_url = self.base_url
        urlpath = ''

        if search:
            self.search_params['_uniqueFormToken'] = search.group(1)
            self.search_params.update({
                '_qf__listingFilter': '',
                'filterSubmitHidden': '1',
                'searchType': '2',
                'adCount': '200',
                'postedBy': '',
                'duration': '0',
                'order': '0',
                'type': '2'
            })

            if regions and self.location:
                regions = regions.extract()[0]
                regions = re.findall(r"\['location'\] *= *(\[[\W\w\s]*?\])", regions)
                regions = json.loads(regions[0].replace("'", "\""))[0]

                for region, cities in regions.items():
                    for province, city in cities.items():
                        if city.find(self.location) > -1:
                            urlpath = province
                            break

                    if urlpath:
                        break

                search_url = self.base_url % ("/province/%s" % urlpath.replace(' ', '+'))
                self.search_params['province'] = province

            if not urlpath:
                if 'keywords' in self.search_params:
                    self.search_params['keywords'] += ' %s' % self.location
                else:
                    self.search_params['keywords'] = self.location

                log.msg("Location %s not found. Appending search terms." % self.location)

            yield FormRequest(
                search_url,
                formdata=self.search_params,
                callback=self.parse_search
            )


    def parse_search(self, response):
        sel = Selector(response)
        # Only 1000 items shown
        try:
            results = sel.xpath("//h3[@class='listingsPageTitle']/text()").extract()
            results = float(re.search(r"(\d+) Results", results[0]).group(1))

            log.msg("%d search results. Up to 1000 items are shown." % results)

            results = 1000.0 if results > 1000.0 else results
            results = int(ceil(results/200))
        except:
            results = 1

        for page in xrange(1, results):
            url = urljoin(response.url, "?next=%s" % (page * 200 + 1))

            yield Request(url, callback=self.parse_page)

        for request in self.parse_page(response):
            yield request


    def parse_page(self, response):
        sel = Selector(response)

        for listing in sel.xpath("//a[@class='listingLink']"):
            title = listing.xpath('@title').extract()[0]
            href = listing.xpath('@href').extract()[0]

            self.numresults += 1

            if not self.exclude_listing(title):
                href = urljoin(response.url, href)

                yield Request(href, callback=self.parse_listing)
            else:
                log.msg("Exclude listing: %s" % title)

        log.msg("Result count: %s" % self.numresults)


    def parse_listing(self, response):
        return


    def parse_baseitem(self, loader):
        loader.add_xpath('id', "//strong[@class='kvKey']"\
            "[text()='Ad ID:']/../span/text()")

        loader.add_xpath('url', "//input[@name='adShortURL']/@value")
        loader.add_xpath('title', "//h3[@itemprop='name']/text()")
        loader.add_xpath('description', "//div[@itemprop='description']/node()")
        loader.add_xpath('price', "//span[@itemprop='price']/text()")

        loader.add_xpath('address', "//strong[@class='kvKey']"\
            "[text()='Address:']/../span/text()")

        loader.add_xpath('poster', "//div[@class='adSellerTextMeta']"\
            "//*[self::a or self::span]/text()")

        loader.add_xpath('images', "//div[@id='galleria']/a/@href")

        return loader.load_item()


    @staticmethod
    def get_addetails_xpath(to_find, response):
        sel = Selector(response)
        headings = sel.xpath("//table[contains(@class, 'propertyAdDetails')]//th")
        xpaths = {}

        for index, heading in enumerate(headings.extract()):
            for key in to_find:
                if key in heading:
                    xpaths[key] = "//table[contains(@class,'propertyAdDetails')]"\
                        "//td[%s]/text()" % (index + 1)
                    break

        return xpaths


    def exclude_listing(self, value):
        value = REMOVE_NONALPHA.sub(" ", value)

        for exclude_word in self.exclude_words:
            if value.lower().find(exclude_word) > -1:
                return True

        return False