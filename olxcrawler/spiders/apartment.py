from olxcrawler.spiders import OlxBase
from scrapy.selector import Selector

from olxcrawler.items import ApartmentItemLoader


class Apartment(OlxBase):
    """ OLX.ph crawler for apartment listings
        scrapy crawl apartment
        -a location=Makati
        -a cond=new
        -a price=10000-20000
        -a floor=20-30
        -a beds=1
        -a baths=1
    """

    name = 'apartment'

    start_urls = ["http://www.olx.ph/index.php/select+location+category/"\
        "urlParam/view+category/id/25/name/Apartment+and+Condominium"]

    base_url = "http://www.olx.ph/index.php/view+category/id/25%s/"\
        "Apartment+and+Condominium"


    def __init__(self, *args, **kwargs):
        super(Apartment, self).__init__(*args, **kwargs)

        if 'floor' in kwargs:
            floor = (kwargs['floor'] + '-').split('-')[0:2]
            self.search_params['floorAreaFrom'] = floor[0]
            self.search_params['floorAreaTo'] = floor[1]

        if 'beds'in kwargs:
            self.search_params['beds'] = kwargs['beds']

        if 'baths'in kwargs:
            self.search_params['baths'] = kwargs['baths']

        if 'cond'in kwargs:
            condition = {'new': '1', 'preowned': '4', 'foreclosed': '3'}
            self.search_params['condition'] = condition[kwargs['cond']]

        self.search_params['categoryType'] = '2'


    def parse_listing(self, response):
        sel = Selector(response)
        loader = ApartmentItemLoader(selector=sel)
        to_find = ['Bedrooms', 'Bathrooms', 'Floor Area']
        xpaths = self.get_addetails_xpath(to_find, response)

        loader.add_xpath('condition', "//span[@itemprop='itemCondition']/text()")
        loader.add_xpath('floorarea', xpaths.get('Floor Area'))
        loader.add_xpath('beds', xpaths.get('Bedrooms'))
        loader.add_xpath('baths', xpaths.get('Bathrooms'))

        return self.parse_baseitem(loader)