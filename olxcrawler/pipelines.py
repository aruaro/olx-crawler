# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy import log
from scrapy.exceptions import DropItem


class ApartmentFilterPipeline(object):
    def process_item(self, item, spider):
        if 'description' in item:
            if spider.exclude_listing(item['description']):
                raise DropItem("Exclude listing: %s" % item['title'])

        log.msg("Scraped listing: %s" % item['title'])

        return item