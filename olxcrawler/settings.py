# -*- coding: utf-8 -*-

# Scrapy settings for olxcrawler project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

import os

BOT_NAME = 'olxcrawler'

SPIDER_MODULES = ['olxcrawler.spiders']
NEWSPIDER_MODULE = 'olxcrawler.spiders'


ITEM_PIPELINES = {
    'olxcrawler.pipelines.ApartmentFilterPipeline': 1000,
}

FEED_FORMAT = 'json'
FEED_URI = 'file://' + os.getcwd() + '/output/%(name)s-%(time)s.json'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'olxcrawler (+http://www.yourdomain.com)'
