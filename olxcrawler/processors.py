import re

from scrapy.contrib.loader.processor import MapCompose, Identity, Join


class Uniquify(MapCompose):
    def __call__(self, value, loader_context=None):
    # Like MapCompose, but makes the elements unique
        values = super(Uniquify, self).__call__(value, loader_context)
        return filter(None, list(set(values)))


class StripFirst(object):
    def __call__(self, values):
        for value in values:
            value = value.strip()
            if value is not None and value != '':
                return value

class StripJoin(Join):
    def __call__(self, values):
        return self.separator.join([value.strip() for value in values])

class Numberize(object):
    def __call__(self, values):
        numberized_values = []
        for value in values:
            number = re.sub(r"[^0-9.]", "", value).rstrip(".")

            if number:
                numberized_values.append(number)

        return numberized_values