# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib.loader import ItemLoader
from olxcrawler.processors import *


class OlxcrawlerItem(Item):
    id = Field()
    url = Field()
    title = Field()
    description = Field()
    price = Field()
    images = Field()
    address = Field()
    poster = Field()


class ApartmentItem(OlxcrawlerItem):
    condition = Field()
    floorarea = Field()
    beds = Field()
    baths = Field()


class ApartmentItemLoader(ItemLoader):
    default_item_class = ApartmentItem
    default_output_processor = StripFirst()

    description_in = Identity()
    description_out = StripJoin()

    images_in = Identity()
    images_out = list

    price_in = Numberize()

    poster_out = StripJoin(separator=' - ')

    floorarea_in = Numberize()

    beds_in = Numberize()

    baths_in = Numberize()