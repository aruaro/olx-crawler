# OLX Crawlers #

A set of OLX (equivalent of Craigslist) crawlers for aparment listings, cars, buy and sell products, etc.

Uses Scrapy 0.24 (http://scrapy.org/)

### To crawl apartment listings ###

```
scrapy crawl apartment -a location=Makati -a price=7000-10000 -a exclude="pre selling,tower,high rise"
```

#### Possible args for all types of listings ####

* `-a location=[location]` -  any major city in Philippines
* `-a keyword=[keyword]` - search keywords
* `-a exclude=[exclude]` - don't scrape the item if title or description has an excluded keyword (comma separated)
* `-a price=[min]-[max]` - price range of item 


#### Possible apartment crawler args ####

* `-a cond=[new|preowned|foreclosed]` - apartment condition
* `-a floor=[min]-[max]` - floor area range
* `-a beds=[beds]` - number of bedrooms
* `-a baths=[baths]` - number of bathrooms

### Sample Output ###

```
{
    "description": " <a href=\"http://www.olx.ph/index.php/classifieds+directory/q/the+beacon\" title=\"Buy and Sell Brand New and 2ndhand The Beacon - For Sale Philippines\" class=\"discreetLink\" target=\"_blank\" onclick=\"_gaq.push(['_trackEvent', 'Classifieds', 'Internal Link']);\">The Beacon</a> Makati Condo <br> Studio Bare Starts at 13, 500 <br> Semi Furnished Starts at 14k <br> Location: <br> Walking distance to Greenbelt <br> Makati Medical <br> Makati CBD <br> Don Bosco <br> Waltermart <br> Assumption <br> Little Tokyo <br> Full Amenities <br> Resort type swimming pool <br> Bi Level Gymn <br> Sauna <br> Sky Lounge <br> Barbeque Pits <br> And Many More <br> Pls contact the number above for further details <br> SMS/ Viber/Line/ Tango/ Whatsapp <br> Kindly look for Ms Bella",
    "title": "The Beacon Makati Condo ( Rent Studio w aircon & ref)",
    "url": "http://olx.ph/55541504",
    "poster": "BellaUA5 -  - Carmina Ilano",
    "price": "15000.00",
    "baths": "1",
    "floorarea": "21",
    "address": "ARNAIZ AVE ( PASAY ROAD) COR CHINO ROCES (PASONG TAMO) NEAR AMORSOLO ST",
    "images": [
        "http://cdn07.olx.ph/20140609003150-232ad3b8d37d483effe8918a43c91ece.jpg",
        "http://cdn09.olx.ph/20140609002328-92d2e2b016cf6f7e618fcb9c2e4d175a.jpg",
        "http://cdn10.olx.ph/20140609002935-42ec03308a15f662dd712bd38d1b43d5.jpg",
        "http://cdn05.olx.ph/20140609003041-d501b417f26d1717147178f5ef360c93.jpg",
        "http://cdn09.olx.ph/20140609003112-a0b8194033db2e76a4ec3fffd9f2f97d.jpg",
        "http://cdn01.olx.ph/20140609003227-4d77f4bd7e4d2ffb467da4822f5edaa0.jpg",
        "http://cdn04.olx.ph/20140609003249-3b1b949dc69e12992065249fce96d6ef.jpg",
        "http://cdn08.olx.ph/20140609003426-fe870a8fe1eb8df8307fc22fb1af6528.jpg"
    ],
    "id": "55541504",
    "condition": "New"
}
```