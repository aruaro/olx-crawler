<?php

error_reporting(E_ALL);

ini_set('display_errors', 'on');
ini_set('display_startup_errors', 'on');

require 'vendor/autoload.php';

if(isset($_GET['o'])) {
    $rows = json_decode(file_get_contents('output/' . $_GET['o'] . '.json'), true);
    $headers = array_keys(current($rows));

    foreach($rows as $row) {
        foreach($row as $key => $value) {
            if($key == 'images');
        }
    }

    $m = new Mustache_Engine();

    echo $m->render(file_get_contents('template.html'), array(
         'headers' => $headers,
         'rows'    => $rows
    ));

}
?>